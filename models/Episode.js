import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const episodeSchema = new Schema({
  title: String,
  description: String,
  audioUrl: String,
  artwork: String,
  season: Number,
  episode: Number,
  day: Number,
  month: Number,
  year: Number,
  hour: Number,
  minute: Number,
  keywords: [String],
  podcast: {
    type: ObjectId,
    ref: 'Podcast'
  }
},
{
  timestamps: true
});

const Episode = mongoose.model('Episode', episodeSchema);

module.exports = Episode;