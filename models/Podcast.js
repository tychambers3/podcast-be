import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const podcastSchema = new Schema({
  title: String,
  description: String,
  logo: String,
  coverPhoto: String,
  subscribers: 0,
  likes: 0,
  comments: [{
    type: ObjectId,
    ref: 'Comment'
  }],
  episodes: [{
    type: ObjectId,
    ref: 'Episode'
  }],
  contributors: [{
    type: ObjectId,
    ref: 'Contributor'
  }],
  owner: {
    type: ObjectId,
    ref: 'User'
  }
}, {
  timestamps: true
});

const Podcast = mongoose.model('Podcast', podcastSchema);

module.exports = Podcast;