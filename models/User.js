import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const userSchema = new Schema({
  firstName: String,
  lastName: String,
  username: String,
  email: String,
  password: String,
  podcasts: [{
    type: ObjectId,
    ref: 'Podcast'
  }]
}, {
  timestamps: true
});

const User = mongoose.model("User", userSchema);

module.exports = User;