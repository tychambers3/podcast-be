import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import config from './config';
import mongoose from 'mongoose';

import auth from './api/auth';
import podcasts from './api/podcasts';
import episodes from './api/episodes';

const database = config[config.env].database;

const db = mongoose.connection;
const app = express();
const port = process.env.PORT || '3001';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));
app.use(cors());

mongoose.connect(database, {useNewUrlParser: true});

db.on('error', console.error.bind(console, "Connection error"));
db.once('open', () => console.log("Connected sucessfully to database"));
 
app.use(express.static('assets'));

app.use('/api/auth', auth);
app.use('/api/podcasts', podcasts);
app.use('/api/episodes', episodes);

app.listen(port, () => console.log("App running on " + port));