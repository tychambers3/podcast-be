import express from 'express';
import { authHandler } from '../libs/middleware';
import Podcast from '../models/Podcast';
import User from '../models/User';

const router = express.Router();

router.post('/new', authHandler, async (req, res) => {
  try {
    const { title, description } = req.body;
    const owner = res.locals.userId;
    const user = await User.findOne({_id: res.locals.userId});

    const podcast = await Podcast.create({
      title,
      description,
      owner
    });

    user.save(err => {
      if ( err ) throw new Error(err);

      user.podcasts.push(podcast._id);
      user.save();
    });

    res.send(podcast);
  }

  catch(err) {
    console.log(err);
  }
});

router.get('/:user_id/all', authHandler, async (req, res) => {
  try {
    const userId = req.params.user_id;

    const podcasts = await Podcast.find({owner: userId});
    
    res.send(podcasts);
  }

  catch(err) {
    console.log(err);
  }
});

router.get('/:user_id/podcasts/:id', authHandler, async (req, res) => {
  try { 
    const userId = req.params.user_id;
    const podcastId = req.params.id;
    const user = await User.findOne({_id: userId}).populate('podcasts');

    const podcast = await Podcast.findOne({owner: user._id, _id: podcastId});
    
    res.send(podcast);
  }

  catch(err) {
    console.log(err);
  }
});

module.exports = router;