import express from 'express';
import { authHandler } from '../libs/middleware';
import User from '../models/User';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import config from '../config';

const router = express.Router();

router.post('/signup', async (req, res) => {
  try {
    const email = req.body.email;
    const password = req.body.password;
    const hashPassword = bcrypt.hashSync(password, 8);

    const user = await User.create({
      email,
      password: hashPassword 
    });

    const token = await jwt.sign({id: user._id, email: user.email}, config.development.secret, {
      expiresIn: "30d"
    });


    res.json({
      user,
      token
    });
  }

  catch(err) {
    console.log(err);
  }
});

router.get('/', authHandler, async (req, res) => {
  try {
    const user = await User.findOne({_id: res.locals.userId}, { password: 0 }).populate('podcasts');
    
    res.send(user);
  }

  catch(err) {
    console.log(err);
  }
});

module.exports = router;