import express from 'express';
import Episode from '../models/Episode';
import { authHandler } from '../libs/middleware';
import Podcast from '../models/Podcast';

const router = express.Router();

router.get('/:podcast_id/episodes', async (req, res) => {
  try {
    const podcastId = req.params.podcast_id;

    const episodes = await Episode.find({podcast: podcastId});

    res.send(episodes);
  }

  catch(err) {
    console.log(err);
  }
});

router.post('/new', authHandler, async (req, res) => {
  try {
    const { title, description, url, season, episode, keywords, day, month, year, hour, minute, audioFile, artwork } = req.body;
    const podcastId = req.query.id;
    const podcast = await Podcast.findOne({_id: podcastId});
    
    const episodeModel = await Episode.create({
      title,
      description,
      url,
      season,
      episode,
      day,
      month,
      year,
      hour,
      minute,
      audioFile,
      artwork
    });

    episodeModel.save(err => {
      if ( err ) throw new Error(err);
      
      episodeModel.keywords.push(keywords);
      episodeModel.save();
    });

    podcast.save(err => {
      if ( err ) throw new Error(err);

      podcast.episodes.push(episode._id);
      podcast.save();
    });

    res.send(episodeModel);
  }

  catch(err) {
    console.log(err);
  }
});

module.exports = router;